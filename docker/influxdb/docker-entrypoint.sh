#!/bin/bash

DB_ENDPOINT='http://localhost:8086'

/entrypoint.sh $1 &

echo "Waiting InfluxDB to start"

cmd="curl ${DB_ENDPOINT}/ping"
while : ; do
    $cmd
    [[ $? -eq 0 ]] && break
	sleep 5;
done

echo "Adding JMeter database"

curl -isS -XPOST "${DB_ENDPOINT}/query" --data-urlencode "q=CREATE DATABASE jmeter"

wait $!

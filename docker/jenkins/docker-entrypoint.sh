#!/bin/bash 

/usr/local/bin/jenkins.sh &
pid=$!

JENKINS_USER=${JENKINS_ADMIN_LOGIN:-admin};
JENKINS_PASSWORD=${JENKINS_ADMIN_PASSWORD:-admin};
JENKINS_HOST="localhost";
JENKINS_PORT="8080";
JENKINS_API="http://$JENKINS_USER:$JENKINS_PASSWORD@$JENKINS_HOST:$JENKINS_PORT";
JENKINS_CLI=/tmp/jenkins-cli.jar
JOBS_PATH=/tmp/jobs

echo "Start downloading Jenkins cli."
while : ; do
    curl -sSL --fail ${JENKINS_API}/jnlpJars/jenkins-cli.jar -o ${JENKINS_CLI} &>/dev/null
    [[ $? -eq 0 ]] && break
    echo "API isn't ready. Sleep 10."
    sleep 10;
done
echo "Finished downloading Jenkins cli."

echo "Start importing jobs to Jenkins."
for job in `ls -1 ${JOBS_PATH}/*.xml`; do
    JOB_NAME=$(basename ${job} .xml)
    java -jar ${JENKINS_CLI} -s $JENKINS_API list-jobs | grep -qw $JOB_NAME
    if [ $? -eq 0 ]; then
        echo "$JOB_NAME exists, updating..."
        cat ${job} | java -jar ${JENKINS_CLI} -s $JENKINS_API update-job ${JOB_NAME}
    elif [ $? -eq 1 ]; then
        echo "${job} is not exists. Creating..."
        java -jar ${JENKINS_CLI} -s $JENKINS_API create-job $JOB_NAME < ${job}
        if [ $? -eq 0 ]; then
            echo "Job $JOB_NAME successfuly added."
        else
          echo "Job $JOB_NAME was not imported: error code $?"
        fi
    fi
done
echo "Finished importing jobs to Jenkins."

wait $pid

# Performance-Testing
----

Project could be used for backend load testing with Apache JMeter. Real-time test metrics should be available in Grafana 'Apache JMeter Dashboard'. Dashboard contains visualizations based on data from InfluxDB.  

Services:
* Apache JMeter
* Jenkins
* InfluxDB
* Grafana

### Start
```bash
docker-compose pull
docker-compose up -d
```

### Stop
```bash
docker-compose down
```

### To run test plan
1. Place *.jmx files into ```./docker/jmeter/scenarios```
2. Go to Jenkins URL;  
3. Choose 'backend' job --> Build with Parameters;
4. Selet test plan *.jmx file --> Build.

Open Grafana dashboard to see test metrics. To retrieve test results in html and jtl formats execute next command:
```bash
docker cp jenkins:/var/jenkins_home/Results ./TestResults
```

### Services endpoints  
* Jenkins: http://localhost:8081      
* Grafana: http://localhost:3000  

<details>
  <summary>Credentials</summary>
    <p>UN: admin</p>
    <p>PW: admin</p>
</details>

### Build docker images
```bash
docker-compose build
```

### Real-time metrics
![](docs/img/live_metrics.png)
